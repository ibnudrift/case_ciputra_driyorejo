@extends('layouts.front')

@section('content-kategori-active')
<p><i class="icon-sarana"></i> &nbsp;DIVISI {{ $parent_cat->title }}</p>
@endsection

@section('content-sidebar-link')
<li>
    <a href="{{ url('/list/' . '?id='.$kategori->id.'&parent='.$parent_cat->id.'&type=list' ) }}"><i class="fas fa-book"></i> &nbsp;Daftar Isi Buku</a>
</li>
<li>
    <a href="{{ url('/list/' . '?id='.$kategori->id.'&parent='.$parent_cat->id.'&type=video' ) }}"><i class="fas fa-video"></i> &nbsp;Daftar Isi Video</a>
</li> 
@endsection

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-11">
            
            <nav aria-label="breadcrumb" class="blocks_breadcrumbs">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/home' ) }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/landing/' . '?id='.$parent_cat->id ) }}">{{ $parent_cat->title }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $data->title }}</li>
              </ol>
            </nav>
            {{-- start block list --}}
             <div class="card default_card block-details-post">
                <div class="card-body">
                    <div class="clear height-5"></div>
                    {{-- Start body --}}
                    <div class="padding-def-content detail-content-post">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <h6 class="category">{{ $kategori->title_kategori }}</h6>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="text-right">
                                <span class="dates"><i class="fa fa-calendar"></i> &nbsp;{{ date('d - m - Y', strtotime($data->created_at)) }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="clear height-5"></div>
                        <div class="block-titles">
                            <h2 class="title">{{ $data->title }}</h2>
                        </div>
                        <div class="clear height-10"></div>
                        @if ($data->video != '')
                        <div class="frame_video">
                            <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://youtube.com/embed/{{ $data->video }}?rel=0&controls=0&autoplay=1&showinfo=0&mute=0&loop=1' frameborder='0' allowfullscreen></iframe></div>
                        </div>
                        @endif

                        {!! $data->content !!}
                        <div class="clear height-20"></div>
                        <div class="dropdown-divider"></div>
                        <div class="clear height-10"></div>
                        <div class="blocks_back_article">
                            <a href="{{ url('/list/' . '?id='.$kategori->id.'&parent='.$parent_cat->id.'&type=list' ) }}" class="btn btn-link"><i class="fa fa-chevron-left"></i> &nbsp;Back</a>
                        </div>
                        <div class="clear height-30"></div>
                        <div class="clear"></div>     
                    </div>

                    {{-- End Body --}}
                    <div class="clear"></div>
                </div>
            </div>
            {{-- end block list --}}

            <div class="clear"></div>
        </div>
    </div>    

</div>
@endsection
