@extends('layouts.front')

@section('content-kategori-active')
<p><i class="icon-sarana"></i> &nbsp;DIVISI {{ $parent_cat->title }}</p>
@endsection

@section('content-sidebar-link')
<li>
    <a href="{{ url('/list/' . '?id='.$kategori->id.'&parent='.$parent_cat->id.'&type=list' ) }}"><i class="fas fa-book"></i> &nbsp;Daftar Isi Buku</a>
</li>
<li>
    <a href="{{ url('/list/' . '?id='.$kategori->id.'&parent='.$parent_cat->id.'&type=video' ) }}"><i class="fas fa-video"></i> &nbsp;Daftar Isi Video</a>
</li> 
@endsection

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-11">
            
            {{-- start block list --}}
             <div class="back-to-listing pb-2">
                 <a href="#" onclick="window.history.back()"><i class="fa fa-chevron-left"></i> Kembali</a>
             </div>
             <div class="card default_card">
                <div class="card-header">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <h5>DATA ARSIP</h5>
                        </div>
                        {{-- <div class="col-md-5">
                            <div class="filter-showsdata">
                                <form class="form-inline" method="GET" action="{{ route('list') }}">
                                    <label class="mr-sm-2" for="inlineFormInputName2">Group Panduan</label>
                                      <input type="hidden" name="id" value="{{ $kategori->id }}">
                                    <select class="onselects_change_group custom-select mr-sm-2" id="inlineFormInputName2" name="group" data-kategori="{{ $kategori->id }}">
                                        <option selected>-- Pilih Group --</option>
                                        @foreach ($tgroup as $keys => $data_nm)
                                            @if ($keys == $ac_tgroup))
                                            <option selected="selected" value="{{ $keys }}">{{ $data_nm }}</option>
                                            @else
                                            <option value="{{ $keys }}">{{ $data_nm }}</option>
                                            @endif
                                        @endforeach
                                      </select>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    &nbsp;&nbsp;
                                    <a href="{{ route('list') }}?id={{ $kategori->id }}"><button type="button" class="btn btn-default">Reset</button></a>
                                </form>
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="card-body">

                    <div class="clear height-5"></div>
                    {{-- Start body --}}
                    <table id="example2" class="table dataTable" role="grid" aria-describedby="example2_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="">No.</th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="">Daftar Isi Buku</th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="">-</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (count($data) > 0)
                                @foreach ($data as $key => $val_el)
                                @php
                                $key = $key + 1;
                                @endphp
                                <tr role="row" class="odd">
                                  <td class="sorting_1">{{ $key }}.</td>
                                  <td>{{ $val_el->title }}</td>
                                  <td><a href="{{ url('/detail/' . '?id='.$val_el->id. '&kategori='.$kategori->id. '&parent='.$parent_cat->id ) }}" data-toggle="tooltip" data-placement="top" title="View Data"><i class="fa fa-eye"></i></a></td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                      </table>

                      <div class="padding-def-content">
                         <div class="row no-gutters d-none">
                            <div class="col-sm-5">
                                <?php echo $data->render(); ?>
                                {{-- <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                                    <nav aria-label="...">
                                      <ul class="pagination pagination-sm">
                                        <li class="page-item disabled">
                                          <a class="page-link" href="#" tabindex="-1">Previous</a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item active">
                                          <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                          <a class="page-link" href="#">Next</a>
                                        </li>
                                      </ul>
                                    </nav>
                                </div> --}}

                            </div>
                            <div class="col-sm-7">
                                {{-- <div class="dataTables_info text-right" id="example2_info" role="status" aria-live="polite"><small>Showing 1 to 10 of 57 entries</small></div> --}}
                            </div>
                        </div>
                    </div>

                    {{-- End Body --}}
                    <div class="clear"></div>
                </div>
            </div>
            {{-- end block list --}}

            <div class="clear"></div>
        </div>
    </div>    

</div>
@endsection
