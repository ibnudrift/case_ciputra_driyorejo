@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Item Pekerjaan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Item Pekerjaan</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">
        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif
        <div class="box">
         <div class="box-header">
            <div class="row">
              <div class="col-md-6">
                <h3>Data Item Pekerjaan</h3>
                <a href="{{ URL::route('subjectc.create') }}" class="btn btn-primary">Tambah Item Pekerjaan</a>
              </div>
              <div class="col-md-6">
                <div class="text-right" style="margin-top: 20px;">
                  <form action="{{ route('admin.searchsubject') }}" method="post" role="search">
                    {{ csrf_field() }}
                      <div class="input-group">
                          <input type="text" class="form-control" name="q"
                              placeholder="Search data" value="@if ($q) {{ $q }} @endif"> <span class="input-group-btn">
                              <button type="submit" class="btn btn-default">
                                  <span class="glyphicon glyphicon-search"></span>
                              </button>
                          </span>
                      </div>
                  </form>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
         </div>
         <div class="box-body">
             @if(count($subjects) > 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Urutan</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($subjects as $data)
                        <tr>
                            <td>{{ $data->title }}</td>
                            <td>{{ $data->sorting }}</td>
                            <td>
                                <a href="{{ URL::route('subjectc.edit',$data->id) }}" class="btn btn-info"></i> Edit</a>
                                <form action="{{action('SubjectcController@destroy', $data->id)}}" method="post" class="hapus" style="display:inline">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
            belum ada data
            @endif
            <?php echo $subjects->render(); ?>
         </div>
         </div>
     </div>
 </div>
<hr>
 
@endsection