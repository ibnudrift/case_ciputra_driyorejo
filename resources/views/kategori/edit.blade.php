@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Kategori
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">

        <div class="box">
         <div class="box-header">
            <h4>Update Kategori</h4>
         </div>
         <div class="box-body">

            @if(isset($kategori))
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/backend/admin/kategori/' . $kategori->id) }}" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('title_kategori') ? ' has-error' : '' }}">
                    <label for="title_kategori" class="col-md-4 control-label">Nama</label>
                    <div class="col-md-6">
                        <input id="title_kategori" type="text" class="form-control" name="title_kategori" value="{{ old('title_kategori', $kategori->title_kategori) }}" required="required">
                        @if ($errors->has('title_kategori'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title_kategori') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('icons') ? ' has-error' : '' }}">
                    <label for="icons" class="col-md-4 control-label">Image Icon</label>
                    <div class="col-md-6">
                        <input id="icons" type="file" class="form-control" name="icons">
                        @if ($errors->has('icons'))
                        <span class="help-block">
                            <strong>{{ $errors->first('icons') }}</strong>
                        </span>
                        @endif
                        @if ($kategori->icons)
                            <span class="help-block" style="max-width: 90px; margin: 1em 0;">
                                <img src="{{ URL::to('/upload/kategori/'.$kategori->icons )}}" alt="" class="img-thumbnail img-fluid">
                            </span>
                        @endif
                        <span class="help-block">*) Note: Kosongi file gambar apabila tidak ingin mengganti gambar, <br>Ukuran file gambar harus 140px X 140px, tipe file png</span>
                    </div>
                </div>
         
                <div class="form-group{{ $errors->has('urutan') ? ' has-error' : '' }}">
                    <label for="urutan" class="col-md-4 control-label">Sorting</label>
                    <div class="col-md-6">
                        <input id="urutan" type="number" class="form-control" name="urutan" value="{{ old('urutan', $kategori->urutan) }}" required="required">
                        @if ($errors->has('urutan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('urutan') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
         
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i> Simpan
                        </button>
                         <a href="{{ URL::route('kategori.index') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                    </div>
                </div>
            </form>
            @endif

            
         </div>
        </div>

 

     </div>
 </div>
<hr>
 
@endsection