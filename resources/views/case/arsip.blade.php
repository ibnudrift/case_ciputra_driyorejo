@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Data Case
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Case</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">
        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif
        <div class="box">
         <div class="box-header">
            <h3>Data Case</h3>
         </div>
         <div class="box-body">

             @if(count($cases) > 0)
                <table class="table lists_case_m">
                    <thead>
                        <tr>
                            <th>Proyek</th>
                            <th>PIC</th>
                            {{-- <th>Item Pekerjaan</th> --}}
                            <th>No Case</th>
                            <th>Status</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($cases as $data)
                        <tr class="{{ $data->status_data }}">
                            <td>{{ $data->nama_proyek }}</td>
                            <td>{{ $data->nama_divisi }}</td>
                            {{-- <td>{{ ucfirst(trans($data->nama_item)) }}</td> --}}
                            <td>{{ $data->no_case }}</td>
                            <td>{{ ($data->status == '1')? "OPEN": "CLOSE" }}</td>
                            <td>-</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
            arsip kosong
            @endif
            <?php echo $cases->render(); ?>
            <div class="clear clearfix" style="height: 45px;"></div>
         </div>
         </div>
     </div>
 </div>
<hr>
 
@endsection