@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Grouping
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Grouping</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">

        <div class="box">
         <div class="box-header">
            <h4>Update Grouping</h4>
         </div>
         <div class="box-body">

            @if(isset($grouping))
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/backend/admin/grouping/' . $grouping->id) }}">
                    <input name="_method" type="hidden" value="PATCH">
                    {{ csrf_field() }}

                    <div class="form-group {{ $errors->has('names') ? ' has-error' : '' }}">
                    <label for="names" class="col-md-4 control-label">Name</label>
                    <div class="col-md-6">
                        <input id="name" type="text" required="required" class="form-control" name="names" value="{{ old('names', $grouping->names) }}" >
                        @if ($errors->has('names'))
                        <span class="help-block">
                            <strong>{{ $errors->first('names') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
             
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i> Simpan
                        </button>
                         <a href="{{ URL::route('grouping.index') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                    </div>
                </div>
            </form>
             
            @endif

            
         </div>
        </div>

 

     </div>
 </div>
<hr>
 
@endsection