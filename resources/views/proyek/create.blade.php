@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Proyek
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Proyek</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">

        <div class="box">
         <div class="box-header">
            <h4>Tambah Proyek</h4>
         </div>
         <div class="box-body">

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/backend/admin/proyek') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                    <label for="nama" class="col-md-4 control-label">Nama Proyek</label>
                    <div class="col-md-6">
                        <input id="nama" type="text" class="form-control" name="nama" value="{{ old('nama') }}" required="required">
                        @if ($errors->has('nama'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
         
                <div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
                    <label for="info" class="col-md-4 control-label">Info Proyek</label>
                    <div class="col-md-6">
                        <textarea name="info" id="info" rows="4" class="form-control" required>{{ old('info') }}</textarea>
                        @if ($errors->has('info'))
                        <span class="help-block">
                            <strong>{{ $errors->first('info') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lokasi') ? ' has-error' : '' }}">
                    <label for="lokasi" class="col-md-4 control-label">Lokasi</label>
                    <div class="col-md-6">
                        <input id="lokasi" type="text" class="form-control" name="lokasi" value="{{ old('lokasi') }}" required="required">
                        @if ($errors->has('lokasi'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lokasi') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i> Simpan
                        </button>
                        <a href="{{ URL::route('proyek.index') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                    </div>
                </div>
            </form>

         </div>
        </div>

 

     </div>
 </div>
<hr>
 
@endsection