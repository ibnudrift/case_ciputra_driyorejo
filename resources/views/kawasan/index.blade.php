@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Kawasan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kawasan</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">
        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif
        <div class="box">
         <div class="box-header">
            <div class="row">
              <div class="col-md-4">
                @if ($filter_proyek)
                <h3>{{ $filter_proyek->nama }} - Kawasan</h3>
                <a href="{{ URL::route('kawasan.create', ['proyek'=> $filter_proyek->id]) }}" class="btn btn-primary">Tambah Kawasan</a>
                @else
                <h3>Data Kawasan</h3>
                @endif

              </div>
              <div class="col-md-8">
                <div class="text-right boxs_filter_proyek_tp" style="margin-top: 20px;">
                  <form action="{{ route('admin.searchkawasan') }}" method="GET" role="search">
                    {{-- {{ csrf_field() }} --}}
                      <div class="d-block align-middle">
                          <div class="d-inline-block align-middle pr-2">
                            <label for="">Pilih Proyek</label>
                          </div>
                          <div class="d-inline-block align-middle pr-2">
                            <select name="proyek" class="form-control" id="filters_proyekfrm">
                              @foreach ($m_proyek as $key => $value)
                              @if ($filter_proyek)
                                <option @if ($value->id == $filter_proyek->id) selected="selected" @endif  value="{{ $value->id }}">{{ucfirst(trans($value->nama))}}</option>
                              @else
                                <option value="{{ $value->id }}">{{ucfirst(trans($value->nama))}}</option>
                              @endif
                              @endforeach
                            </select>
                          </div>
                          @if ($filter_proyek)
                          <div class="d-inline-block align-middle pr-2">
                            <input type="text" class="form-control" name="q"
                                placeholder="Search data" value="@if ($q) {{ $q }} @endif"> <span class="input-group-btn">
                            </span>
                          </div>
                          @endif
                          <div class="d-inline-block align-middle">
                            <button type="submit" class="btn btn-default">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                          </div>
                      </div>
                  </form>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
         </div>
         <div class="box-body">
             @if(count($kawasan) > 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($kawasan as $data)
                        <tr>
                            <td>{{ $data->title }}</td>
                            <td>
                                <a href="{{ URL::route('kawasan.edit',$data->id) }}" class="btn btn-info"></i> Edit</a>
                                <a href="{{ URL::route('admin.tipesindexs', ['proyek'=> $data->proyek_id, 'kawasan'=> $data->id]) }}" class="btn btn-success">View &nbsp;<i class="fa fa-chevron-right fa-1 fz12"></i></a>
                                {{-- <form action="{{action('KawasanController@destroy', $data->id)}}" method="post" class="hapus" style="display:inline">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger" type="submit">Delete</button>
                                </form> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
                <?php echo $kawasan->render(); ?>
            @else
              belum ada data
            @endif
         </div>
         </div>
     </div>
 </div>
<hr>
 
@endsection