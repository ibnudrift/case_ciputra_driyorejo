@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Member
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Member</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">
        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif
        <div class="box">
         <div class="box-header">
            <h3>Data Member</h3>
            <a href="{{ URL::route('member.create') }}" class="btn btn-primary">Tambah Member</a>
         </div>
         <div class="box-body">
             @if(count($member) > 0)
                <table class="table">
                    <thead>
                        <tr>
                            {{-- <th>Nama Lengkap</th> --}}
                            <th>Username</th>
                            {{-- <th>Nomer Karyawan</th> --}}
                            <th>Proyek</th>
                            <th>Role</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($member as $data)
                        <tr>
                            {{-- <td>{{ $data->name }}</td> --}}
                            <td>{{ $data->email }}</td>
                            {{-- <td>{{ $data->nik }}</td> --}}
                            <td>{{ App\Member::proyekName($data->proyek_id) }}</td>
                            <td>
                            @if ($data->role_member == 3)
                              Auditor
                            @elseif ($data->role_member == 2)
                              Manager Divisi
                            @elseif ($data->role_member == 4)
                              GM Proyek
                            @else
                              Pengawas
                            @endif
                            </td>
                            <td>
                                <a href="{{ URL::route('member.edit', $data->id) }}" class="btn btn-info"></i> Edit</a>
                                <form action="{{action('MemberController@destroy', $data->id)}}" method="post" class="hapus" style="display:inline">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger to_delete" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
            belum ada data
            @endif
            <?php echo $member->render(); ?>
         </div>
         </div>
     </div>
 </div>
<hr>
 
 <script type="text/javascript">
  $(function(){

    $("button.to_delete").click(function() {

        var isGood=confirm('Are You Sure?');
        if (isGood) {
          return true
        } else {
          return false;
        }

    });

  });
</script>
@endsection