@extends('template')
 
@section('content')
 
<h3>Data Karyawan</h3>
<hr>
<a href="{{ URL::route('karyawan.create') }}">Tambah Karyawan</a>
<hr>
@if(count($karyawan) > 0)
 
    <table class="table">
        <thead>
            <tr>
                <th>Nama Lengkap</th>
                <th>NIK</th>
                <th>Jabatan</th>
                <th>#</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($karyawan as $data)
            <tr>
                <td>{{ $data->nama_lengkap }}</td>
                <td>{{ $data->NIK}}</td>
                <td>{{ $data->jabatan }}</td>
                <td>
                    <a href="{{ URL::route('karyawan.edit',$data->id) }}" class="btn btn-info btn-sm"></i> Edit</a>
                    <form action="{{action('KaryawanController@destroy', $data->id)}}" method="post" class="hapus" style="display:inline">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@else
belum ada data
@endif
 
 
@stop