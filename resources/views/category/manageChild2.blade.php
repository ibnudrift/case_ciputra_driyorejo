<ul>
@foreach($childs as $child)
	<li>
	    <a href="{{ URL::route('category.edit', ['id' => $child->id]) }}">{{ $child->title }}</a>
	@if(count($child->childs))
            @include('category.manageChild',['childs' => $child->childs])
        @endif
</li>
@endforeach
</ul>