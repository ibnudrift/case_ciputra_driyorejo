@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Kategori
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori</li>
      </ol>
    </section>
@endsection
@section('data-content')
<div class="row">
     <div class="col-xs-12">

        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif

        <div class="row">
          <div class="col-md-6">
            <div class="box">
               <div class="box-header">
                  <h3>Eidt Kategori</h3>
               </div>
               <div class="box-body">
                {!! Form::open(['route'=>'edit.category', 'files' => true]) !!}

                @if ($message = Session::get('success'))
                  <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                  </div>
                @endif
                {!! Form::hidden('id', $ids) !!}
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                  {!! Form::label('Title:') !!}
                  {!! Form::text('title', old('title', $category->title), ['class'=>'form-control','placeholder'=>'Enter Title']) !!}
                  <span class="text-danger">{{ $errors->first('title') }}</span>
                </div>
                <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
                  {!! Form::label('Category:') !!}
                  {!! Form::select('parent_id',$allCategories, old('parent_id', $category->parent_id), ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
                  <span class="text-danger">{{ $errors->first('parent_id') }}</span>
                </div>
                <div class="form-group {{ $errors->has('sorting') ? 'has-error' : '' }}">
                  {!! Form::label('Urutan:') !!}
                  {!! Form::text('sorting', old('sorting', $category->sorting), ['class'=>'form-control','placeholder'=>'Enter Urutan']) !!}
                  <span class="text-danger">{{ $errors->first('sorting') }}</span>
                </div>
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                  {!! Form::label('image', 'Icon Kategori') !!}
                  {!! Form::file('image', ['class'=>'form-control']) !!}
                  <p class="help-block" style="font-size: 11px;">*) Note: picture size width: 140px x height: 140px</p>
                  @if ($category->image != '')
                    <img src="{{ asset('upload/kategori/')}}/{{ $category->image }}" alt="" class="img img-fluid img-thumbnail" style="max-width: 70px;">
                  @endif
                  <span class="text-danger">{{ $errors->first('image') }}</span>
                </div>
              <div class="form-group">
                  <button class="btn btn-success">Simpan</button>
                  <a href="{{ URL::route('category.index') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Kembali</a>
                </div>
              {!! Form::close() !!}

                <div class="clear clearfix"></div>
               </div>
             </div>
          </div>
          <div class="col-md-6">
            <div class="box">
               <div class="box-header">
                  <h3>List Kategori</h3>
               </div>
               <div class="box-body">
                <div class="tree block-section">
                  <ul id="tree1">
                    @foreach($categories as $category)
                        <li>
                            <a href="{{ URL::route('category.edit', ['id' => $category->id]) }}">{{ $category->title }}</a>
                            @if(count($category->childs))
                                @include('category.manageChild', ['childs' => $category->childs])
                            @endif
                        </li>
                    @endforeach
                  </ul>
              </div>

               </div>
             </div>
          </div>
        </div>
        {{-- End rows snData --}}

     </div>
 </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="{{ asset('asset/js/treeview.js') }}"></script>
@endsection