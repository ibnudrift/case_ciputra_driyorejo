CREATE VIEW `view_case_divisi` AS
select 
`list_case`.`id` as `case_id`, 
`list_case`.`nama`, 
`list_case`.`no_case`, 
`proyek`.`nama` as `nama_proyek`, 
`division`.`name_departement` as `nama_divisi`, 
`list_case`.`penyebab`, 
`list_case`.`cara_perbaikan`, 
`list_case`.`foto_sebelum`, 
`list_case`.`foto_sesudah`, 
`list_case`.`status`, 
`subjects`.`title` as `subject`, 
`list_case`.`content` as `content`,
(CASE WHEN ( `list_case`.`date_selesai` = CURDATE() AND date_reschedule >=  CURDATE() AND `list_case`.`status` = 2 ) THEN "d_selesai" 
WHEN ( (`list_case`.`date_selesai` - CURDATE()) <= 2 AND (`list_case`.`date_selesai` - CURDATE()) >= 1 ) THEN "d_warning"
WHEN (`list_case`.`date_selesai` <= CURDATE()) THEN "d_warning2"
WHEN (`list_case`.`date_selesai` < CURDATE()) THEN "d_terlambat" 
ELSE "d_progress" END)
 as `status_data`,
`division`.`id` as `divisi_id`,
`division`.`name_departement` as `divisi_name`
from `division` 
left join `list_case` on `list_case`.`divisi_id` = `division`.`id` 
left join `proyek` on `list_case`.`proyek_id` = `proyek`.`id` 
left join `subjects` on `list_case`.`item_pk_id` = `subjects`.`id` 
where (`list_case`.`is_archive` = 0 and `list_case`.`proyek_id` = 30)
group by `division`.`id`
order by `division`.`id` ASC;