<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Proyek;
use Auth;

class ProyekController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index()
    {
        $proyek = Proyek::orderBy('id', 'desc')->paginate(10);
        return view('proyek.index', ['proyek' => $proyek]);
    }

    public function create()
    {
        return view('proyek.create');
    }

    public function edit($id) {
        $proyek = Proyek::findOrFail($id);
        return view('proyek.edit', ['proyek' => $proyek]);
    }
 
    public function store(Request $request) {
        Proyek::create($request->all());
        \Session::flash('notifikasi', 'Data berhasil ditambah.');
        return redirect('backend/admin/proyek');
    }
 
    public function update(Request $request, $id) {
        $proyek = Proyek::findOrFail($id);
        $proyek->update($request->all());
        \Session::flash('notifikasi', 'Data berhasil diubah.');
        return redirect('backend/admin/proyek');
    }
 
    public function destroy($id) {
        Proyek::destroy($id);
        \Session::flash('notifikasi', 'Data berhasil dihapus.');
        return redirect('backend/admin/proyek');
    }

}
