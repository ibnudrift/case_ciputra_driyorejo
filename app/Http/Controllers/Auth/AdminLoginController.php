<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
class AdminLoginController extends Controller
{

use AuthenticatesUsers;

    protected $redirectTo = '/backend';

	public function __construct(){
		$this->middleware('guest:admin')->except('logout');
	}


    public function showLoginForm(){
    	return view('auth.admin-login');
    }

   public function guard()
   {
    return Auth::guard('admin');
   }

   protected function attemptLogin(Request $request)
    {
      $remember = $request->has('remember') ? true : false;
      // Check with email
      $loginWithEmail = $this
        ->guard('admin')
        ->attempt($request->only(['email','password']),$remember);
        // dd($loginWithEmail);
      if(!$loginWithEmail){
        $request->request->add(['username' => $request->email]);
        $request->request->remove('email');
        return $this
        ->guard()
        ->attempt($request->only(['username','password']),$remember);
      }

      return $loginWithEmail;
    }
    
    public function logout()
    {
        
        $this->guard()->logout();

        return redirect('/backend/login');

    }

    // public function login(Request $request){
    //     dd($request->all());
    // 	//return true;
    // 	$this->Validate($request, [
    // 		'email'=> 'required|email',
    // 		'password'=>'required|min:6'
    // 	]);

    // 	if(Auth::guard('admin')->attempt(['email' => $request->email , 'password' => $request->password], $request->remember)){
    // 		return redirect()->intended(route('admin.dashboard'));
    // 	}
    // 	return redirect()->back()->withInput($request->only('email', 'remember'));
    // }
}