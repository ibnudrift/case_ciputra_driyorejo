<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Kawasan;
use App\Proyek;
use App\Tipes;
use Auth;

class TipesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {
        $tipes = array();

        if ( isset($_GET['proyek']) ) {
            $filters_id = $_GET['proyek'];
            $kawasan_id = $_GET['kawasan'];
            $tipes = Tipes::where([
				    ['proyek_id', '=', intval($filters_id)],
				    ['kawasan_id', '=', intval($kawasan_id)],
				])
            ->orderBy('id', 'desc')
            ->paginate(50);
        }

        $filter = '';
        $m_proyek = Proyek::all();

        if (isset($_GET['proyek'])) {
            $filters_id = $_GET['proyek'];
            $f_proyek = Proyek::where('id', intval($filters_id) )->first();
            $filter = $f_proyek;
        }

        $m_kawasan = Kawasan::all();
        if (isset($_GET['kawasan'])) {
            $filters_id = $_GET['kawasan'];
            $f_kawasan = Kawasan::where('id', intval($filters_id) )->first();
            $filter_kawasan = $f_kawasan;
        }

        return view('tipes.index', [
			        	'tipes' => $tipes, 
			        	'q'=>array(), 
			        	'filter_proyek'=> $filter, 
			        	'm_proyek'=> $m_proyek,
			        	'm_kawasan'=> $m_kawasan,
			        	'filter_kawasan'=> $filter_kawasan, 
			        ]);
    }

    // public function searching(Request $request) {
    //     $q = htmlspecialchars($request->q, ENT_QUOTES);

    //     $kawasan = Tipes::orderBy('id', 'desc')
    //                 ->where('title','LIKE','%'.$q.'%')
    //                 ->paginate(10);
    //     return view('tipes.index', ['kawasan' => $kawasan, 'q'=> $q]);
    // }
 
    public function create() {
        $proyek = 0;
        if (isset($_GET['proyek'])) {
            $proyek = intval($_GET['proyek']);
            $f_proyek = Proyek::where('id', intval($proyek) )->first();
            $filter = $f_proyek;
        }

        $kawasan = 0;
        if (isset($_GET['kawasan'])) {
            $kawasan = intval($_GET['kawasan']);
            $f_kawasan = Kawasan::where('id', intval($kawasan) )->first();
            $filter_kawasan = $f_kawasan;
        }
        
        return view('tipes.create', ['proyek'=> $proyek, 'filter_proyek'=> $filter, 'kawasan'=> $kawasan,'filter_kawasan'=> $filter_kawasan]);
    }
 
    public function edit($id) {
        $tipes = Tipes::findOrFail($id);

        if (isset($tipes->proyek_id)) {
            $proyek = intval($tipes->proyek_id);
            $f_proyek = Proyek::where('id', intval($proyek) )->first();
            $filter = $f_proyek;

            $f_kawasan = Kawasan::where('id', intval($tipes->kawasan_id) )->first();
            $filter_kawasan = $f_kawasan;
        }

        return view('tipes.edit', ['tipes' => $tipes, 'proyek'=> $proyek, 'filter_proyek'=> $filter, 'filter_kawasan'=> $filter_kawasan]);
    }
 
    public function store(Request $request) {
        Tipes::create($request->all());
        \Session::flash('notifikasi', 'Data berhasil ditambah.');

        return \Redirect::route('admin.tipesindexs', ['proyek'=> $request->proyek_id, 'kawasan'=>$request->kawasan_id]);
    }
 
    public function update(Request $request, $id) {
        $kawasan = Tipes::findOrFail($id);
        $kawasan->update($request->all());

        \Session::flash('notifikasi', 'Data berhasil diubah.');
        return \Redirect::route('admin.tipesindexs', ['proyek'=> $kawasan->proyek_id, 'kawasan'=>$request->kawasan_id]);
    }
 
    public function destroy($id) {
        $in_proyek = 0;
        $kawasan = Tipes::findOrFail($id);
        $in_proyek = $kawasan->proyek_id;
        Tipes::destroy($id);

        \Session::flash('notifikasi', 'Data berhasil dihapus.');
        return \Redirect::route('admin.tipesindexs', ['proyek'=> $in_proyek, 'kawasan'=>$kawasan->kawasan_id]);
    }

    public function show($id) {

    }

}
