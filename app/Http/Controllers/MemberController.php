<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Member;
use App\Divisi;
use App\Proyek;
use Auth;

class MemberController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {
        $member = Member::orderBy('id', 'desc')->paginate(15);
        return view('member.index', ['member' => $member]);
    }
 
    public function create() {
        $v_div = Divisi::pluck('name_departement', 'id')->all();
        $v_proyek = Proyek::pluck('nama', 'id')->all();
        return view('member.create', ['divisi' => $v_div, 'proyek'=>$v_proyek]);
    }
 
    public function edit($id) {
        $member = Member::findOrFail($id);
        $v_div = Divisi::pluck('name_departement', 'id')->all();
        $v_proyek = Proyek::pluck('nama', 'id')->all();
        return view('member.edit', ['member' => $member, 'divisi'=>$v_div, 'proyek'=>$v_proyek]);
    }
 
    public function store(Request $request) {
        // Member::create($request->all());
        $request->validate([
        // 'name' => 'required',
        'email' => 'required|max:18',
        'password' => 'required',
        // 'division' => 'required',
        // 'id_member' => 'required',
        // 'nik' => 'required|max:15',
        'proyek_id' => 'required',
        'role_member' => 'required',
        ]);

        $members = new Member;
        // $members->name = $request->input('name');
        $members->email = $request->input('email');
        $members->password = bcrypt($request->input('password'));
        // $members->division = $request->input('division');
        $members->nik = $request->input('nik');
        $members->proyek_id = $request->input('proyek_id');
        $members->role_member = $request->input('role_member');
        $members->save();

        \Session::flash('notifikasi', 'Data berhasil ditambah.');
        return redirect('backend/admin/member');
    }
 
    public function update(Request $request, $id) {
        $member = Member::findOrFail($id);

        // $member->update($request->all());
        $pass_lama = $member->password;

        $request->validate([
        // 'name' => 'required',
        'email' => 'required|max:18',
        'password' => 'nullable|max:255',
        // 'division' => 'required',
        // 'nik' => 'required|max:15',
        'proyek_id' => 'required',
        'role_member' => 'required',
        ]);

        $data = array(
                    // 'name'=> $request->input('name'),
                    'email'=> $request->input('email'),
                    'password'=> ($request->input('password') != '')? bcrypt($request->input('password')) : $pass_lama,
                    // 'division'=> $request->input('division'),
                    'nik'=> $request->input('nik'),
                    'proyek_id'=> $request->input('proyek_id'),
                    'role_member'=> $request->input('role_member'),
                    );
        Member::where('id' , $id)->update($data);

        \Session::flash('notifikasi', 'Data berhasil diubah.');
        return redirect('backend/admin/member');
    }
 
    public function destroy($id) {
        Member::destroy($id);
        \Session::flash('notifikasi', 'Data berhasil dihapus.');
        return redirect('backend/admin/member');
    }
}
