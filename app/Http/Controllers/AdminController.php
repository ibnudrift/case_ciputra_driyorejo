<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Model;

use App\Admin;
use App\Member;
use App\Mcase;

use Auth;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index()
    {
        $all_divisi = Member::where('role_member', 1)->get();

        $results = [];
        foreach ($all_divisi as $key => $value) {

        // Get user staff
        $sub_model = Mcase::select( DB::raw('list_case.id, list_case.nama, list_case.no_case, list_case.penyebab, list_case.cara_perbaikan, list_case.foto_sebelum, list_case.foto_sesudah, list_case.status, list_case.content as content') )
            ->where([
                    ['list_case.is_archive', '=', 0],
                    ['list_case.proyek_id', '=', 1],
                    ['list_case.divisi_id', '=', $value->id]
                    ])
            ->get();

            $open = 0;
            $close = 0;
            $nls = [];
            foreach ($sub_model as $keys => $values) {
                $nls[] = $values->status;
            }
            $ls_res_data = array_count_values($nls);

            $results[] = [
                        'divisi_id' => $value->id,
                        'divisi_name' => $value->email,
                        'status' => $ls_res_data,
                        'total_data' => count($sub_model),
                        'open'=>isset($ls_res_data[1])? $ls_res_data[1] : 0,
                        'close'=>isset($ls_res_data[2])? $ls_res_data[2] : 0,
                        ];
        }

        return view('admin', [ 'n_model'=>$results ]);
    }

    public function index2()
    {
        $admin = Admin::all();
        return view('admins.index', ['admin' => $admin]);
    }

    public function postInsert(Request $req)
    {
        //$this ->validate($req , ['tipe_admin'=> 'required' , 'keterangan'=> 'required']);
        //return 'Validation Pass';

        $req->validate([
        'nama' => 'required',
        'username' => 'required',
        'password' => 'required',
        'email' => 'required',
        ]);

        //DB::table('admin')->insert(['tipe_admin'=>$req->input('tipe'),
        //              'keterangan'=>$req->input('keterangan')]);
        //return redirect('/admin/admin')->with('info','Insert Success!');
        
        $admin = new Admin;
        $admin->nama = $req->input('nama');
        $admin->username = $req->input('username');
        $admin->password = bcrypt($req->input('password'));
        $admin->email = $req->input('email');
        $admin->save();

        \Session::flash('notifikasi', 'Data berhasil ditambah!.');
        return redirect('backend/admin/admins');
    }

    public function edit($id)
    {
        $adminedit = Admin::find($id);
        return view('admins.editadmin', ['adminedit' => $adminedit]);
    }

    public function update(Request $request , $id)
    {
        $admins = Admin::findOrFail($id);
        $pass_lama = $admins->password;

        $request->validate([
            'nama' => 'required',
            'username' => 'required',
            'password' => 'max:255',
            'email' => 'required',
            ]);
        $data = array(
                    'nama'=> $request->input('nama'), 
                    'username' => $request-> input('username'), 
                    'email' => $request-> input('email'),
                    'password'=> ($request->input('password') != '')? bcrypt($request->input('password')) : $pass_lama,
                );
        
        Admin::where('id_admin' , $id)->update($data);

        \Session::flash('notifikasi', 'Data berhasil diupdate!.');
        return redirect('backend/admin/admins');

    }
    public function delete($id)
    {
        Admin::where('id_admin', $id)->delete();
        \Session::flash('notifikasi', 'Data berhasil dihapus!.');
        return redirect('backend/admin/admins');
    }
}

