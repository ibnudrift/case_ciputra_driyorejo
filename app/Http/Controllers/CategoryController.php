<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;
use Auth;

class CategoryController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index()
    {
        $categories = Category::where('parent_id', '=', 0)->get();
        $allCategories = Category::pluck('title','id')->all();

        return view( 'category.index', compact('categories', 'allCategories') );
    }

    public function edit()
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        }else{
            return redirect('backend/admin/category/index');
        }
        
        $categories = Category::where('parent_id', '=', 0)->get();
        $allCategories = Category::pluck('title','id')->all();

        $category = Category::findOrFail($id);
        $ids = $id;

        return view('category.edit', compact('category', 'categories', 'allCategories', 'ids'));
    }

    public function delete() {
        $id = htmlentities( intval($_GET['id']) );
        Category::destroy($id);
        \Session::flash('notifikasi', 'Data berhasil dihapus.');
        return redirect('backend/admin/category/index');
    }

    public function manageCategory()
    {
        $categories = Category::where('parent_id', '=', 0)->get();
        $allCategories = Category::pluck('title','id')->all();

        return view('category.categoryTreeview',compact('categories','allCategories'));
    }

   /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function addCategory(Request $request)
    {
        $this->validate($request, [
        		'title' => 'required',
        	]);
        $input = $request->all();
        $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];
        
        Category::create($input);
        return back()->with('success', 'New Category added successfully.');
    }

    public function editData(Request $request)
    {
        $kategori = Category::findOrFail($request->id);
        $save_image = $kategori->image;

        if ($request->file('image')) {
        
            $this->validate($request, [
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $image = $request->file('image');
            $input['imagename'] = time() . '_'. rand(100, 3000) .'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('upload/kategori');
            $image->move($destinationPath, $input['imagename']);

            $all_input = $request->all();
            $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];
            $data_update = array_merge($all_input, ['image' => $input['imagename']]);
        } else {
            $all_input = $request->all();
            $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];
            $data_update = array_merge($all_input, ['image' => $save_image]);
        }

        $kategori->update($data_update);
        return back()->with('success', 'Edit Category has been updated.');

    }

    public function treeView(){ 

        $Categorys = Category::where('parent_id', '=', 0)->get();
        $tree='<ul id="browser" class="filetree">';
        foreach ($Categorys as $Category) {
             $tree .='<li class="tree-view closed"><a class="tree-name">'.$Category->title.'</a>';
             if(count($Category->childs)) {
                $tree .=$this->childView($Category);
            }
        }
        $tree .='<ul>';
        return $tree;

        return view('category.treeview', compact('tree'));
    }

    public function childView($Category){                 
            $html ='<ul>';
            foreach ($Category->childs as $arr) {
                if(count($arr->childs)){
                $html .='<li class="tree-view closed"><a class="tree-name">'.$arr->title.'</a>';                  
                        $html.= $this->childView($arr);
                    }else{
                        $html .='<li class="tree-view"><a class="tree-name">'.$arr->title.'</a>';                                 
                        $html .="</li>";
                    }
                                   
            }
            
            $html .="</ul>";
            return $html;
    }    

}
