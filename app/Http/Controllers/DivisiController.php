<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Divisi;
use Auth;

class DivisiController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {
        $divisi = Divisi::orderBy('id', 'desc')->paginate(10);
        return view('divisi.index', ['divisi' => $divisi]);
    }
 
    public function create() {
        return view('divisi.create');
    }
 
    public function edit($id) {
        $divisi = Divisi::findOrFail($id);
        return view('divisi.edit', ['divisi' => $divisi]);
    }
 
    public function store(Request $request) {
        Divisi::create($request->all());
        \Session::flash('notifikasi', 'Data berhasil ditambah.');
        return redirect('backend/admin/divisi');
    }
 
    public function update(Request $request, $id) {
        $divisi = Divisi::findOrFail($id);
        $divisi->update($request->all());
        \Session::flash('notifikasi', 'Data berhasil diubah.');
        return redirect('backend/admin/divisi');
    }
 
    public function destroy($id) {
        Divisi::destroy($id);
        \Session::flash('notifikasi', 'Data berhasil dihapus.');
        return redirect('backend/admin/divisi');
    }
}
