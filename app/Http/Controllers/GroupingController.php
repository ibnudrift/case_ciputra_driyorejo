<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Grouping;
use Auth;

class GroupingController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {
        $groping = Grouping::orderBy('id', 'asc')->paginate(10);
        return view('grouping.index', ['grouping' => $groping]);
    }
 
    public function create() {
        return view('grouping.create');
    }
 
    public function edit($id) {
        $groping = Grouping::findOrFail($id);
        return view('grouping.edit', ['grouping' => $groping]);
    }
 
    public function store(Request $request) {
        Grouping::create($request->all());
        \Session::flash('notifikasi', 'Data berhasil ditambah.');
        return redirect('backend/admin/grouping');
    }
 
    public function update(Request $request, $id) {
        $groping = Grouping::findOrFail($id);
        $groping->update($request->all());
        \Session::flash('notifikasi', 'Data berhasil diubah.');
        return redirect('backend/admin/grouping');
    }
 
    public function destroy($id) {
        Grouping::destroy($id);
        \Session::flash('notifikasi', 'Data berhasil dihapus.');
        return redirect('backend/admin/grouping');
    }
}
