<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use App\ListAktivitas;
use App\Member;
use App\Proyek;
use Auth;

class ListaktivitasController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {
        $n_member = Member::pluck('name', 'id')->all();
    	$n_proyek = Proyek::pluck('nama', 'id')->all();
        return view('listaktivitas.index', ['member' => $n_member, 'proyek' => $n_proyek]);
    }

    public function changeProyek(Request $request)
    {
        $id_proyek = $request->input('id_proyek');
        $get_memb = Member::where('proyek_id', $id_proyek)
                        ->orderBy('name', 'asc')
                        ->get();
        $str = '';
        if (count($get_memb) > 0) {
            foreach ($get_memb as $key => $value) {
                $str .= '<option value="'.$value->id.'">'.ucwords($value->name).'</option>';
            }
        }
        return $str;
    }

    public function showdata(Request $request)
    {
    	$request->validate([
        'user_id' => 'required',
        ]);
        $request->validate(['user_id' => 'required']);
    	$ids_user = $request->input('user_id');

        $result = DB::table('activity_logs')
             ->select('user_id', 'post_id', DB::raw('count(post_id) as jumlah'), DB::raw('posts.title as name_artikel'), 'activity_logs.created_at as tanggal_input')
             ->where('user_id', $ids_user)
             ->groupBy('post_id')
             ->join('posts', 'activity_logs.post_id', '=', 'posts.id')
             ->get();

        $member = Member::findOrFail($ids_user);
        $proyek = Proyek::findOrFail($member->proyek_id);
        return view('listaktivitas.show', ['data' => $result, 'member'=>$member, 'proyek'=>$proyek]);
    }

    public function showDetail()
    {
        $ids_user = Input::get('user_id');
        $ids_post = Input::get('post_id');

        $result = DB::table('activity_logs')
             ->select('user_id', 'post_id', DB::raw('posts.title as name_artikel'), 'activity_logs.created_at as tanggal_input')
             ->where('user_id', $ids_user)
             ->where(
                [
                    ['user_id', '=', $ids_user],
                    ['post_id', '=', $ids_post],
                ]
             )
             ->join('posts', 'activity_logs.post_id', '=', 'posts.id')
             ->get();

        $member = Member::findOrFail($ids_user);
        $proyek = Proyek::findOrFail($member->proyek_id);

        return view('listaktivitas.show2', ['data' => $result, 'member'=>$member, 'proyek'=>$proyek]);
    }

}
