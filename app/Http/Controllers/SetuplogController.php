<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Setuplog;
use Auth;

class SetuplogController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {
        $setuplog = Setuplog::orderBy('id', 'asc')->paginate(10);
        return view('setuplog.index', ['setuplog' => $setuplog]);
    }
 
    public function create() {
        return view('setuplog.create');
    }
 
    public function edit($id) {
        $setuplog = Setuplog::findOrFail($id);
        return view('setuplog.edit', ['setuplog' => $setuplog]);
    }
 
    public function store(Request $request) {
        Setuplog::create($request->all());
        \Session::flash('notifikasi', 'Data berhasil ditambah.');
        return redirect('backend/admin/setuplog');
    }
 
    public function update(Request $request, $id) {
        $setuplog = Setuplog::findOrFail($id);
        $setuplog->update($request->all());
        \Session::flash('notifikasi', 'Data berhasil diubah.');
        return redirect('backend/admin/setuplog');
    }
 
    public function destroy($id) {
        Setuplog::destroy($id);
        \Session::flash('notifikasi', 'Data berhasil dihapus.');
        return redirect('backend/admin/setuplog');
    }

}
