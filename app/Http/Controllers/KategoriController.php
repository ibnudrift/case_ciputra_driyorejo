<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Model;

use App\Kategori;
use Auth;

class KategoriController extends Controller
{
	public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {
        $kategori = Kategori::all();
        return view('kategori.index', compact('kategori'));
    }
 
    public function create() {
        return view('kategori.create');
    }
 
    public function edit($id) {
        $kategori = Kategori::findOrFail($id);
        return view('kategori.edit', compact('kategori'));
    }
 
    public function store(Request $request) {

        $this->validate($request, [
            'icons' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        // echo $icons->getClientOriginalName();
        $icons = $request->file('icons');
        $input['imagename'] = time() . '_'. rand(100, 3000) .'.'.$icons->getClientOriginalExtension();
        $destinationPath = public_path('upload/kategori');
        $icons->move($destinationPath, $input['imagename']);

        $data_insert = array_merge($request->all(), ['icons' => $input['imagename']]);

        Kategori::create( $data_insert );
        \Session::flash('notifikasi', 'Kategori berhasil ditambah.');
        return redirect('backend/admin/kategori');
    }
 
    public function update(Request $request, $id) {
        $kategori = Kategori::findOrFail($id);
        $save_image = $kategori->icons;

        if ($request->file('icons')) {
        
            $this->validate($request, [
                'icons' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $icons = $request->file('icons');
            $input['imagename'] = time() . '_'. rand(100, 3000) .'.'.$icons->getClientOriginalExtension();
            $destinationPath = public_path('upload/kategori');
            $icons->move($destinationPath, $input['imagename']);

            $data_update = array_merge($request->all(), ['icons' => $input['imagename']]);
        } else {
            $data_update = array_merge($request->all(), ['icons' => $save_image]);
        }

        $kategori->update($data_update);
        \Session::flash('notifikasi', 'Kategori berhasil diubah.');
        return redirect('backend/admin/kategori');
    }
 
    public function destroy($id) {
        Kategori::destroy($id);
        \Session::flash('notifikasi', 'Kategori berhasil dihapus.');
        return redirect('backend/admin/kategori');
    }

}
