<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Model;

use App\Kategori;
use App\Posts;
use App\ListAktivitas;

class ViewSOPController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!isset($_GET['id']) AND !isset($_GET['kategori'])) {
            abort(403, 'Unauthorized action.');
        }

        $id_kat = isset($_GET['kategori'])? $_GET['kategori']: '';
        $kategori = Kategori::findOrFail($id_kat);

        $post_id = isset($_GET['id'])? $_GET['id']: '';
        $post = Posts::findOrFail($post_id);
        
        // type artikel / video
        return view('viewsop.detail', ['data'=>$post, 'kategori'=> $kategori]);
    }

}