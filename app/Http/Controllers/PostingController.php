<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Posts;
use App\Kategori;
use App\Grouping;
use App\Category;
use Auth;

class PostingController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {        
        $order = 'asc';
        $posts = Posts::leftJoin('categories', 'posts.category_id', '=', 'categories.id')->orderBy('posts.id', $order)->select( DB::raw('posts.id, posts.title, posts.views, categories.title as names_category') )->paginate(15);

        // $posts = Posts::orderBy('id', 'asc')->paginate(15);

        if (isset($_GET['kategori']) ) {
             
             // $posts = Posts::leftJoin('grouping', 'posts.tgroup', '=', 'grouping.id')
             // ->orderBy('grouping.names', $order)
             // ->select( DB::raw('posts.id, posts.title, posts.views, grouping.names as names_group') )
             // ->where('tgroup', '=', $_GET['group'])
             // ->paginate(50);
            $cat_id = htmlentities( intval($_GET['kategori']) );
            // get cat id sub categories
            $sn_arr1 = DB::table('categories')->select('id')->where('parent_id',  $cat_id)->get();
            $res_ar1 = array();
            if (count($sn_arr1) > 0) {
                foreach ($sn_arr1 as $key => $value) {
                    $res_ar1[] = $value->id;
                }
            }
            $res_ar1[] = $cat_id;

            $order = 'asc';
            $posts = Posts::leftJoin('categories', 'posts.category_id', '=', 'categories.id')->orderBy('posts.id', $order)->select( DB::raw('posts.id, posts.title, posts.views, categories.title as names_category') )->whereIn('posts.category_id', $res_ar1)->paginate(15);

        }
        $s_actgroup = isset($_GET['kategori'])? $_GET['kategori'] : 0;

        if (isset($_GET['search']) ) {
             $posts = Posts::leftJoin('grouping', 'posts.tgroup', '=', 'grouping.id')
             ->orderBy('grouping.names', $order)
             ->select( DB::raw('posts.id, posts.title, posts.views, grouping.names as names_group') )
             ->where('title', 'like', '%' . $_GET['search'] . '%')
             ->paginate(50);
        }
        $s_actsearch = isset($_GET['search'])? $_GET['search'] : '';

        $allgroup = Category::getListOptionMenu();
        // echo "<pre>";
        // print_r($allgroup);
        // echo "</pre>";
        // exit;

        return view('posting.index', ['posts' => $posts, 'allgroup'=> $allgroup, 'ac_group'=> $s_actgroup, 'ac_search'=> $s_actsearch]);
    }
 
    public function create() {
        // $kategori = Kategori::pluck('title_kategori', 'id')->all();
        $kategori = Category::getListOptionMenu();
    	$tgroup = Grouping::pluck('names', 'id')->all();
        return view('posting.create', ['kategori' => $kategori, 'tgroup'=> $tgroup]);
    }
 
    public function edit($id) {
        $posts = Posts::findOrFail($id);
        // $kategori = Kategori::pluck('title_kategori', 'id')->all();
        $kategori = Category::getListOptionMenu();
        $tgroup = Grouping::pluck('names', 'id')->all();
        return view('posting.edit', ['posts' => $posts, 'kategori'=>$kategori, 'tgroup'=> $tgroup]);
    }
 
    public function store(Request $request) {

        $merge = array_merge($request->all(), ['admin_id' => Auth::user()->id_admin]);
        
        Posts::create( $merge );
        \Session::flash('notifikasi', 'Data berhasil ditambah.');
        return redirect('backend/admin/posting');
    }
 
    public function update(Request $request, $id) {
        $posts = Posts::findOrFail($id);

        $merge = array_merge($request->all(), ['admin_id' => Auth::user()->id_admin]);
        // print_r($merge); exit;
        $posts->update($merge);
        \Session::flash('notifikasi', 'Data berhasil diubah.');
        return redirect('backend/admin/posting');
    }
 
    public function destroy($id) {
        Posts::destroy($id);
        \Session::flash('notifikasi', 'Data berhasil dihapus.');
        return redirect('backend/admin/posting');
    }
}
