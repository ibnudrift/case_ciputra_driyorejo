<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Subjects;
use Auth;

class SubjectcController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {
        $subjects = Subjects::orderBy('sorting', 'asc')->paginate(10);
        return view('subjectc.index', ['subjects' => $subjects, 'q'=>array()]);
    }

    public function searching(Request $request) {
        $q = htmlspecialchars($request->q, ENT_QUOTES);

        $subjects = Subjects::orderBy('sorting', 'asc')->where('title','LIKE','%'.$q.'%')->paginate(10);
        return view('subjectc.index', ['subjects' => $subjects, 'q'=> $q]);
    }
 
    public function create() {
        return view('subjectc.create');
    }
 
    public function edit($id) {
        $subjects = Subjects::findOrFail($id);
        return view('subjectc.edit', ['subjects' => $subjects]);
    }
 
    public function store(Request $request) {
        Subjects::create($request->all());
        \Session::flash('notifikasi', 'Data berhasil ditambah.');
        return redirect('backend/admin/subjectc');
    }
 
    public function update(Request $request, $id) {
        $subjects = Subjects::findOrFail($id);
        $subjects->update($request->all());
        \Session::flash('notifikasi', 'Data berhasil diubah.');
        return redirect('backend/admin/subjectc');
    }
 
    public function destroy($id) {
        Subjects::destroy($id);
        \Session::flash('notifikasi', 'Data berhasil dihapus.');
        return redirect('backend/admin/subjectc');
    }

    public function show($id) {

    }

}
