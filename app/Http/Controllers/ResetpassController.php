<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;


class ResetpassController extends Controller
{

    public function resetpass()
    {
        // Validate the new password length...
        DB::table('admin')
            ->where('id_admin', 1)
            ->update([
                'password' => Hash::make('123456')
            ]);

        echo 'done';
        die(0);
    }


}