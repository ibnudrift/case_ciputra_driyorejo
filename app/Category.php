<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $fillable = ['title','parent_id', 'image', 'sorting'];
    /**
     * Get the index name for the model.
     *
     * @return string
    */

    public function childs() {
        return $this->hasMany('App\Category', 'parent_id','id')->orderBy('sorting');
    }

    public static function getListOptionMenu($parent_id = 0)
    {
    	$parent_id = htmlentities( intval($parent_id) );
    	$result = '';
    	$resource = Category::where('parent_id', '=', $parent_id)->get();
    	foreach ($resource as $key => $value) {
    		$result .= $value->id.'||'.$value->title."\n";
    		// Sub 1
    		if (count($value->childs) > 0) {
    			foreach ($value->childs as $key1 => $value1) {
    				$result .= $value1->id.'||- '.$value1->title."\n";
    				// Sub 2
    				if (count($value1->childs) > 0) {
		    			foreach ($value1->childs as $key2 => $value2) {
		    				$result .= $value2->id.'||-- '.$value2->title."\n";
		    				// Sub 3
		    				if (count($value2->childs) > 0) {
				    			foreach ($value2->childs as $key3 => $value3) {
				    				$result .= $value3->id.'||--- '.$value3->title."\n";
				    			}
				    		}
		    			}
		    		}
    			}
    		}
    	}

    	$s_res_array = array_filter( explode("\n", $result) );

    	$data = array();
    	foreach ($s_res_array as $key => $value) {
    		$sn_data = array_filter(explode('||', $value));
    		$sns_id = $sn_data[0];
    		$data[$sns_id] = $sn_data[1];
    	}

        return $data;
    }


}
