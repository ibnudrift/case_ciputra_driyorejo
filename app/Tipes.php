<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipes extends Model
{
    protected $table = 'tipes';
    protected $fillable = [
    					'nama', 
    					'kawasan_id', 
    					'proyek_id'
						];
}
