<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setuplog extends Model
{
     protected $table = 'setuplog';
    protected $fillable = [
    					'id', 
    					'logs', 
    					'status'
						];
}
