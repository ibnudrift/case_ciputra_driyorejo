<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Proyek as Proyek;

class Member extends Model
{
    protected $table = 'users';
    protected $fillable = ['name', 'email', 'password', 'division', 'nik', 'proyek_id','role_member'];

    public static function proyekName($id)
    {
        $model = Proyek::where('id', $id)->get()->first();
        return $model['nama'];
    }
}
